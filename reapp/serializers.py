from .models import Location
from .models import ReferredUser
from rest_framework import serializers



class ReferedSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReferredUser
        fields=('first_name','email_address')


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model =Location
        fields =('location','is_active')
