from django.http import Http404
from .models import ReferredUser, Location
from rest_framework.views import APIView
from .serializers import *
from rest_framework.response import Response
from rest_framework import status
from django_filters.rest_framework import filters
from rest_framework.throttling import UserRateThrottle,ScopedRateThrottle
from project import settings

class ContactListView(UserRateThrottle):
    scope = 'contacts'

class ExceptionalUserRateThrottle(UserRateThrottle):

    def allow_request(self, request, view):
        """
        Give special access to a few special accounts.

        Mirrors code in super class with minor tweaks.
        """
        if self.rate is None:
            return True

        self.key = self.get_cache_key(request, view)
        if self.key is None:
            return True

        self.history = self.cache.get(self.key, [])
        self.now = self.timer()

        # Adjust if user has special privileges.
        override_rate = settings.REST_FRAMEWORK['DEFAULT_THROTTLE_RATES'].get(
            request.user.username,
            None,
        )
        if override_rate is not None:
            self.num_requests, self.duration = self.parse_rate(override_rate)

        # Drop any requests from the history which have now passed the
        # throttle duration
        while self.history and self.history[-1] <= self.now - self.duration:
            self.history.pop()
        if len(self.history) >= self.num_requests:
            return self.throttle_failure()
        return self.throttle_success()

class ReferedList(APIView):
    def post(self, request, format=None):

        serializer = ReferedSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LocationsList(APIView):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('location')
    throttle_classes = (ExceptionalUserRateThrottle,)
    def get(self_, request, format=None):
        off = Location.objects.all()
        serializer = LocationSerializer(off, many=True)
        response = {
            "status":'request was permitted',
            "location": [serializer.data]
        }
        return Response(status=status.HTTP_200_OK)

    def post(self, request, format=None):
        serializer = LocationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LocationsDetail(APIView):
    throttle_classes = (ContactListView,)
    
    def get_object(self, pk):
        try:
            return Location.objects.get(pk=pk)
        except Location.DoesNotExist:
            return Http404

    def get(self, request, pk, format=None):
        offers = self.get_object(pk)
        serializer = LocationSerializer(offers)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        offers = self.get_object(pk)
        serializer = LocationSerializer(offers, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        offers = self.get_object(pk)
        offers.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


