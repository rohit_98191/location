from django.db import models
from django.core.validators import RegexValidator



class Location(models.Model):
    location = models.CharField(max_length=20)
    is_active = models.BooleanField(default=True)


class ReferredUser(models.Model):
    first_name = models.CharField(max_length=30)
    email_address = models.EmailField()
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 10 digits allowed.")
    mobile_no = models.CharField(validators=[phone_regex], max_length=10)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)

