# Generated by Django 2.0.1 on 2018-01-04 07:46

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reapp', '0002_auto_20180102_1828'),
    ]

    operations = [
        migrations.AlterField(
            model_name='referreduser',
            name='mobile_no',
            field=models.CharField(max_length=10, validators=[django.core.validators.RegexValidator(message="Phone number must be entered in the format: '+999999999'. Up to 10 digits allowed.", regex='^\\+?1?\\d{9,15}$')]),
        ),
    ]
